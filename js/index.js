$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()

    $('#exampleModal2').on('show.bs.modal', function (e) {
        //  comienza a abrirse el modaL
        console.log("El modal se Abre");
        // inhabiltar el boton y cambiar color
        $('#btn-con').removeClass('btn-primary');
        $('#btn-con').addClass('btn-secondary');
        $('#btn-con').prop('disabled', true);


      })

    $('#exampleModal2').on('shown.bs.modal', function (e) {
        //  Se ha abierto EL MODAL
        console.log("El modal se ha abierto");
      })

    $('#exampleModal2').on('hide.bs.modal', function (e) {
        //  Se esta cerrando EL MODAL
        console.log("El modal se esta cerrando");
      })

    $('#exampleModal2').on('hidden.bs.modal', function (e) {
        //  Se ha cerrado EL MODAL
        console.log("El modal se ha cerrado");
        // habiltar el boton y cambiar color originalmente
        $('#btn-con').removeClass('btn-secondary');
        $('#btn-con').addClass('btn-primary');
        $('#btn-con').prop('disabled', false);
      })
  })