﻿# Step by step
## learning NodeJs and Bootstrap

# 1. NodeJS

Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.

### Check Nodejs version

```sh
nodejs --version
```

```sh
nodejs -v
```

### Install Nodejs

[https://nodejs.org/](https://nodejs.org/)

# 2. NPM

npm is the default package management system for Node.js, a runtime environment for JavaScript, under Artistic License 2.0.

### Check Nodejs version

```sh
npm -v
```

```sh
npm --version
```

### Install NPM

by default it's included in Nodejs installer

```sh
npm install npm -g 
```

# 3. Create package.json

```sh
npm init
```
example:
```sh
$ npm init
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (webservertest)
version: (1.0.0)
description: test project
entry point: (index.js) index.html
test command:
git repository: (https://agzsoftsi@bitbucket.org/agzsoftsi/webservertest.git)
keywords:
author: Carlos Garcia
license: (ISC)
About to write to C:\Users\agzso\Desktop\Bitbucket\webservertest\package.json:

{
  "name": "webservertest",
  "version": "1.0.0",
  "description": "test project",
  "main": "index.html",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://agzsoftsi@bitbucket.org/agzsoftsi/webservertest.git"
  },
  "author": "Carlos garcia\u001b[D\u001b[D\u001b[D\u001b[D\u001b[G",
  "license": "ISC",
  "homepage": "https://bitbucket.org/agzsoftsi/webservertest#readme"
}


Is this OK? (yes)
```

4. Configure Server - lite-server

### Install Server

```sh
npm install lite-server --save-dev
```

### add a "script" entry within your project's package.json file:

# Inside package.json...
  "scripts": {
    "dev": "lite-server"
  },

### Run server

```sh
npm run dev
```

# 4. Install Bootstrap

```sh
npm install bootstrap --save
```

### Install Bootstrap Dependencies

```sh
npm install jquery --save
```

```sh
npm install popper.js --save
```

